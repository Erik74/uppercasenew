#include <stdio.h>
#include "uppercase.h"

int main(int argc, char *argv[])
{
    char *string = argv[1];
    if(!string){
        printf("Numbers of argument:%d\n",argc);
        printf("Please write a string!");
        return 0;
        }
    stringUpper(string);

    printf("String after stringUpper : %s",string);
    return 0;
}
